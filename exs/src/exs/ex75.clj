(defn totient
  [x]
  (let [gcd (fn [a b] (if (= 0 b) a (gcd b (rem a b))))
        coprime? (fn [a b] (= 1 (gcd a b)))]
  (if (= 1 x)
    x
    (->> (range x)
         (filter #(coprime? % x))
         count))))
