(ns exs.core)
(use 'clojure.tools.trace)

(defn anagram? [w1 w2]
  (let [rember (fn [a lat] 
                 (let [[n m] 
                       (split-with #(not= a %) lat)]
                   (concat n (rest m))))]
    (if (empty? w1)
      true
      (and (= (count w1) (count w2))
           (.contains w2 (str (first w1)))
           (anagram? (apply str (rest w1))
                     (apply str (rember (first w1) w2)))))))
;;(trace-vars anagram?)
(defn anagrams-in-list
  ([w low] (anagrams-in-list w low []))
  ([w low ret]
   (cond (empty? low) (into #{} ret)
         (anagram? w (first low)) 
          (anagrams-in-list w (rest low) (conj ret (first low)))
          :else (anagrams-in-list w (rest low) ret))))

;; The end value! :)
(set (filter #(> (count %) 1) (map #(anagrams-in-list % low) low)))
