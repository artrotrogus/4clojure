(def test1 "4,5,6,7,8,9")
(def test2 "15,16,25,36,37")

(defn sep-perf-squares [string]
  (let [nass (clojure.string/split string #",")
        nasn (map #(Integer/parseInt %) nass)
        square? (fn [n] (let [sqrt (Math/sqrt n)]
                          (== (Math/floor sqrt) sqrt)))
        filtered (filter square? nasn)]
    (clojure.string/join "," filtered)))
