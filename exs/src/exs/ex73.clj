(def board [[:e :e :x] [:o :x :o] [:x :e :o]])
(def board1 [[:e :e :x] [:o :o :o] [:x :e :o]])
(def board2 [[:e :e :x] [:e :x :o] [:e :e :o]])
(def board16 [[:e :x :x :o]
              [:x :o :o :e]
              [:x :o :x :x]
              [:o :x :e :x]])

(defn solve [bd]
  (let [one? (partial = 1)
        won? (fn [row] (and (->> row set count one?) (not (= :e (first row)))))
        winner (fn [row] (when (won? row) (first row)))
        dim (count bd)
        rows bd
        get-cols #(apply map vector %)
        cols (get-cols bd)
        diags-idx (get-cols (for [n (range dim)]
                              [[n n] [n (- (dec dim) n)]]))
        nmth #(get (get bd (first %)) (second %))
        diags (vector (map nmth (first diags-idx))
                      (map nmth (second diags-idx)))
        all (concat rows cols diags)]
    (first (filter #(not (nil? %)) (map winner all)))))
