(letfn
  [(foo [x y] #(bar (conj x y) y))
   (bar [x y] (if (> (last x) 10)
                x
                #(foo x (+ 2 y))))]
  (trampoline foo [] 1))

;; This function allows to call two mutually recursive functions without filling the stack
;; trampoline will continue calling the callee function as long as it returns a function!
