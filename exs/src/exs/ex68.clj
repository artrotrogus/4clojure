(defn rem-dup
  ([coll] (rem-dup (rest coll) (first coll) (vector (first coll))))
  ([coll remd ret]
   (if (empty? coll)
     ret
     (let [car (first coll)
           cdr (rest coll)]
       (if (= car remd)
         (rem-dup cdr remd ret)
         (rem-dup cdr car (conj ret car)))))))
